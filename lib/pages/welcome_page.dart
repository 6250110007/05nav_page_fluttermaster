import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  String name, email, phone;

  WelcomePage(
      {Key? key, required this.name, required this.email, required this.phone})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Returning Value'),
          backgroundColor: Colors.green,
        ),
        body: Center(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Data form Screen 1',
                  style: TextStyle(
                    fontSize: 30,
                  ),
                ),
                Text('Name: $name'),
                Text('Email: $email'),
                Text('Phone: $phone'),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.8),
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.green),
                        ),
                        onPressed: () {
                          Navigator.pop(context, 'You select Agree');
                        },
                        child: Text('Agree'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.8),
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.red),
                        ),
                        onPressed: () {
                          Navigator.pop(context, 'You select Disagree');
                        },
                        child: Text('Disagree'),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
