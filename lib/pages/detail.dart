import 'package:flutter/material.dart';
import 'package:navigator_page/model/data_model.dart';
import 'package:navigator_page/pages/welcome_page.dart';

class Detail extends StatefulWidget {
  final DataModel dataModel;
  const Detail({Key? key, required this.dataModel}) : super(key: key);

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  TextEditingController _name = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();

  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Passing Value'),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _name,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Enter your name'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _email,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Enter your email'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _phone,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Enter your phone'),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                _incrementCounter();
                final result =
                    await Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => WelcomePage(
                              name: _name.text,
                              email: _email.text,
                              phone: _phone.text,
                            )));
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text('$result, press button: $_counter'),
                  ),
                );
                if (result == 'You selected Disagree') {
                  _name.clear();
                  _email.clear();
                  _phone.clear();
                }
                ;
              },
              child: Text('Go Next Page'),
            ),
          ],
        ),
      ),
    );
  }
}
